﻿using UnityEngine;
using System.Collections;

public class DestoryOnContact : MonoBehaviour {

   private AudioSource onHit;

   private GameManager gameManager;

   void Awake()
   {
      onHit = GetComponent<AudioSource>();
      var manager = GameObject.FindGameObjectWithTag("GameManager");
      gameManager = manager.GetComponent<GameManager>();
   }

	void OnTriggerEnter2D(Collider2D other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         onHit.Play();
         gameManager.GameOver();
      }    
   }
}
