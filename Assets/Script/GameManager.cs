﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
   public float startWait;
   public float spawnWait;
   public GameObject obstacle,coin;
   public Transform spawnPos;
   public Text scoreText;
   public Text restartText;
   public Text gameoverText;
   public Text coinText;

   public bool gameOver;

   private int scoreMeter;
   private int coinCounter;


   void Awake()
   {
      gameOver = false;
   }

	void Start () {
      gameoverText.text = "";
      restartText.text = "";
      coinText.text = "Coin: 0";
      scoreMeter = 0;
      StartCoroutine(Spawner());
	}

   IEnumerator Spawner()
   {
      yield return new WaitForSeconds(startWait);
      while (!gameOver)
      {
         var posY = Random.Range(2.5f,7.5f);
         var pos = new Vector3(spawnPos.position.x, posY, 0.0f);
         var rotation = new Vector3(0, 0, 90f);
         var coinSpawn = Random.Range(0, 100);
         var coinSpawn2 = Random.Range(0, 100);
         for (int i = 0; i <2; i++)
         {
            Instantiate(obstacle, pos, Quaternion.identity);
            pos = new Vector3(spawnPos.position.x, posY - 10f, 0.0f);
         }
      
         if(coinSpawn > 0 && Time.timeSinceLevelLoad > 3f)
         {
            if (coinSpawn2 > 50)
            {
               pos = new Vector3(spawnPos.position.x, posY - 5f, 0.0f);
            }
            else
            {
               pos = new Vector3(spawnPos.position.x + 3f, Random.Range(-4.5f, 4.5f), 0.0f);
            }
            Instantiate(coin, pos, Quaternion.identity);
         }
       
         yield return new WaitForSeconds(spawnWait);
      }
   }

   void Update()
   {
      UpdateScore();

      if (gameOver && Input.GetKeyDown(KeyCode.R))
      {
            var scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
      }
   }

   public void UpdateCoin(int value)
   {
      coinCounter += value;
      coinText.text = "Coin: " + coinCounter;
   }

     public void UpdateScore()
   {
      if (!gameOver)
      {
         scoreMeter = (int)Time.timeSinceLevelLoad;
      }

      scoreText.text = "Score: " + scoreMeter + " M";
   }

     public void GameOver(){
         gameOver = true;
      gameoverText.text = "Game Over";
      restartText.text = "Press 'R' to restart game";
      }
}
