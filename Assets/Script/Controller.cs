﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

   public Vector2 speed;

   private AudioSource upSound;
   private Rigidbody2D body2d;
   private GameManager gameManager;

	void Awake () {
      upSound = GetComponent<AudioSource>();
      body2d = GetComponent<Rigidbody2D>();
      var manager = GameObject.FindGameObjectWithTag("GameManager");
      gameManager = manager.GetComponent<GameManager>();
   }
	
	// Update is called once per frame
	void Update () {

      if (Input.GetButtonDown("Jump")&&!gameManager.gameOver)
      {
         upSound.Play();
         body2d.velocity = new Vector2(speed.x, speed.y);
      }
	}

   void OnTriggerEnter2D(Collider2D other)
   {
      if (other.CompareTag("Bottom"))
      {
         gameManager.GameOver();
         Destroy(gameObject,2f);
      }
   }
}
