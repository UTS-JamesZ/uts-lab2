﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

   private GameManager gameManager;
   private AudioSource coinSound;

   void Awake()
   {
      coinSound = GetComponent<AudioSource>();
      var manager = GameObject.FindGameObjectWithTag("GameManager");
      gameManager = manager.GetComponent<GameManager>();
   }

   void OnTriggerEnter2D(Collider2D other)
   {
      if (other.CompareTag("Player"))
      {
         gameManager.UpdateCoin(1);
         coinSound.Play();
         gameObject.GetComponent<SpriteRenderer>().enabled = false;
         gameObject.GetComponent<CircleCollider2D>().enabled = false;
         Destroy(gameObject,1f);
      }
   }
}
