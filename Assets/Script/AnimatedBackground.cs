﻿using UnityEngine;
using System.Collections;

public class AnimatedBackground : MonoBehaviour {

   public float moveSpeed;
   public Vector2 speed;
   public float increaseInterval;

   private float time;
   private Vector2 offset;
   private Material material;

   void Start()
   {
      material = GetComponent<Renderer>().material;
      offset = material.GetTextureOffset("_MainTex");
   }

   void Update()
   {
      time += Time.deltaTime;

      if (time >= increaseInterval && moveSpeed < 2)
      {
         moveSpeed *= 1.05f;
         time = 0;
      }

      offset += speed * Time.deltaTime * moveSpeed;
      material.SetTextureOffset("_MainTex", offset);
   }
}
